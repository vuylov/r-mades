<?php

return [
    'sendTo' => 'emotion@r-mades.com',
    'sendFrom' => 'emotion@r-mades.com',
    'messages' => [
        'success' => 'Мы получили вашу заявку и позвоним в ближайшее время.',
        'error' => 'Что-то пошло не так… По техническим причинам заявку мы не получили. Пожалуйста, отправьте еще раз или напишите нам на welcome@r-mades.com. Спасибо!'
    ]
];
