<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yiidreamteam\upload\ImageUploadBehavior;

/**
 * This is the model class for table "employee".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $lastname
 * @property string $post
 * @property string $description
 * @property string $photo
 * @property integer $position
 * @property integer $created_at
 * @property integer $updated_at
 */
class Employee extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => ImageUploadBehavior::class,
                'attribute' => 'photo',
                'filePath' => '@webroot/uploads/image_[[pk]].[[extension]]',
                'fileUrl' => '/uploads/image_[[pk]].[[extension]]'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname'], 'required'],
            [['description'], 'string'],
            [['position', 'created_at', 'updated_at'], 'integer'],
            [['firstname', 'lastname', 'post'], 'string', 'max' => 255],
            ['photo', 'file', 'extensions' => 'jpeg, jpg, gif, png']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'lastname' => Yii::t('app', 'Lastname'),
            'post' => Yii::t('app', 'Post'),
            'description' => Yii::t('app', 'Description'),
            'photo' => Yii::t('app', 'Photo'),
            'position' => Yii::t('app', 'Position'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return string
     */
    public function getFullname()
    {
        return $this->lastname.' '.$this->firstname;
    }
}