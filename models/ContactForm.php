<?php

namespace app\models;

use Yii;
use yii\base\Exception;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class ContactForm extends Model
{
    public $name;
    public $phone;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
        ];
    }

    /**
     * Sends an email to the specified email address using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     * @throws Exception
     */
    public function contact($email)
    {
        $bodyText = $this->name.' хочет разработку. С ним можно связаться по телефону '.$this->phone;
        if ($this->validate()) {
            Yii::$app->mailer->compose()
                ->setTo($email)
                ->setFrom([$email => $this->name])
                ->setSubject('Заявка на разработку!!!')
                ->setTextBody($bodyText)
                ->send();
            return true;
        }
        return false;
    }
}
