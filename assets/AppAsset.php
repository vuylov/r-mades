<?php
namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/iziToast.min.css',
        'css/vendor.css',
        'css/bundle.min.css'
    ];
    public $js = [
        'js/vendor.js',
        'js/bundle.js',
        'js/iziToast.min.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
