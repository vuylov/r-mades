<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee`.
 */
class m171009_063354_create_employee_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee', [
            'id' => $this->primaryKey(),
            'firstname' => $this->string(255)->notNull(),
            'lastname' => $this->string(255)->notNull(),
            'post' => $this->string(255),
            'description' => $this->text(),
            'photo' => $this->string(255),
            'position' => $this->integer(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee');
    }
}
