<?php

namespace app\controllers;

use app\models\Employee;
use Yii;
use yii\base\Exception;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     * @return string|Response
     * @throws Exception
     * @throws \yii\base\ExitException
     */
    public function actionContact()
    {
        if(\Yii::$app->request->isAjax){
            $model = new ContactForm();
            if ($model->load(Yii::$app->request->post()) && $model->contact(\Yii::$app->params['sendTo'])) {
                echo \Yii::$app->params['messages']['success'];
                \Yii::$app->end();
            }
        }
        throw new Exception('Error');
    }

    /**
     * Раздел "Разработка"
     * @return string
     */
    public function actionDevelopment()
    {
        return $this->render('development');
    }

    /**
     * Раздел "О нас"
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about', [
            'employees' => Employee::find()->orderBy('position')->all()
        ]);
    }

    public function actionProducts()
    {
        return $this->render('products');
    }

    public function actionEqueo()
    {
        return $this->render('equeo');
    }

    public function action4kam()
    {
        return $this->render('4kam');
    }

    public function actionMyteam()
    {
        return $this->render('myteam');
    }

    public function actionTakeda()
    {
        return $this->render('takeda');
    }

    public function actionLafarge()
    {
        return $this->render('lafarge');
    }
}
