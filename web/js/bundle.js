 function isScrolledIntoView(elem) {
     var $elem = $(elem);
     var $window = $(window);

     var docViewTop = $window.scrollTop();
     var docViewBottom = docViewTop + $window.height();

     var elemTop = $elem.offset().top;
     var elemBottom = elemTop + $elem.height();

     return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
 }


 if ($('[data-grow]').length > 0) {
     $(window).scroll(function () {
         $('[data-grow]').each(function () {
             if (isScrolledIntoView($(this)) && !$(this).data('rendered')) {
                 var elem = $(this);
                 var limit = parseFloat(elem.data('grow'));
                 var total;
                 Snap.animate(0, limit, function (val) {
                     total = Math.floor(val);
                     elem.children('span').text(total);
                 }, 1000, mina.easeinout);

                 setTimeout(function() {
                    if (total < limit) {
                        elem.children('span').text(limit.toString().replace('.', ','));
                    }
                 }, 1100);
                 elem.data('rendered', true);
             }
         });
     });
     $(window).scroll();
 }
"use strict";

function hideContactUs() {
    $('.main-menu .contact-us-row').fadeOut( "fast", function() {
        $('.main-menu').removeClass('active').removeClass('contact-us');
        $('.main-menu-toggle').removeClass("cross");
        $('.menu-link.active').removeClass('active');
    });
}

$('.show-contact-us').click(function (e) {
    e.preventDefault();
    var link = $(this);
    var targetSelector = '.main-menu .contact-us-row.' + link.data('target');

    $('.menu-link.active').removeClass('active');
    $('.main-menu .contact-us-row').css({
        'display': 'none'
    });
    
    $('.main-menu').addClass('contact-us');
    $(targetSelector).fadeIn( "slow" );
    link.addClass('active');
});

$('.hide-contact-us').click(function (e) {
    e.preventDefault();
    hideContactUs();
});

$('.remove-contact-us').click(function (e) {
    $('.main-menu .contact-us-row').css( {"display": "none"} );
    $('.main-menu').removeClass('contact-us');
    $('.menu-link.active').removeClass('active');
});

$('.main-menu-toggle').click(function () {
    $(this).toggleClass("cross");
    $('.main-menu').toggleClass("active");
    $('.main-content').toggleClass("hidden-xs");
    if (!$(this).hasClass("cross")) {
        hideContactUs();
    }
});
var figuresMobile = {
    circle1: {
        shape: 'circle',
        scale: { 0: 1 },
        top: '25%',
        left: '18%',
        fill: 'none',
        stroke: 'rgba(0,255,255, 1)',
        strokeWidth: { 10: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        radius: 8
    },
    circle2: {
        shape: 'circle',
        scale: { 0: 1 },
        top: '55%',
        left: '22%',
        fill: 'none',
        stroke: { 'magenta': 'rgba(0,255,255, 1)' },
        strokeWidth: { 10: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        radius: 15
    },
    circle3: {
        shape: 'circle',
        scale: { 0: 1 },
        top: '35%',
        left: '98%',
        fill: 'none',
        stroke: 'rgba(0,255,255, 1)',
        strokeWidth: { 10: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        radius: 15
    },
    circle4: {
        shape: 'circle',
        top: '55%',
        left: '85%',
        fill: 'none',
        stroke: { 'magenta': 'rgba(0,255,255, 1)' },
        strokeWidth: 2,
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        radius: 10
    },
    circle5: {
        shape: 'circle',
        scale: { 0: 1 },
        top: '40%',
        left: '75%',
        fill: 'none',
        stroke: '#f45c57',
        strokeWidth: 2,
        strokeDasharray: '100%',
        strokeDashoffset: { '100%': '-100%' },
        radius: 25
    },
    circle6: {
        shape: 'circle',
        top: '75%',
        left: '15%',
        fill: 'none',
        stroke: '#f45c57',
        strokeWidth: 3,
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        radius: 15
    },
    triangle1: {
        shape: 'polygon',
        points: 3,
        top: '5%',
        left: '75%',
        fill: 'none',
        stroke: { 'magenta': 'rgba(0,255,255, 1)' },
        strokeWidth: { 10: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 0: 180 },
        radius: 20
    },
    triangle2: {
        shape: 'polygon',
        points: 3,
        top: '85%',
        left: '85%',
        fill: 'none',
        stroke: { 'magenta': 'rgba(0,255,255, 1)' },
        strokeWidth: { 1: 5 },
        strokeDasharray: '100%',
        strokeDashoffset: { '100%': '-100%' },
        angle: { 0: 180 },
        radius: 10
    },
    triangle3: {
        shape: 'polygon',
        points: 3,
        top: '5%',
        left: '45%',
        fill: 'none',
        stroke: { 'rgba(0,255,255, 1)': 'magenta' },
        strokeWidth: { 5: 1 },
        strokeDasharray: '100%',
        strokeDashoffset: { '100%': '-100%' },
        angle: { 0: 180 },
        radius: 10
    },
    polygon1: {
        shape: 'polygon',
        points: 5,
        top: '25%',
        left: '85%',
        fill: 'none',
        stroke: { 'rgba(0,255,255, 1)': 'magenta' },
        strokeWidth: { 8: 1 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 10: 180 },
        radius: 15
    },
    polygon2: {
        shape: 'polygon',
        points: 5,
        top: '70%',
        left: '98%',
        fill: 'none',
        stroke: 'rgba(0,255,255, 1)',
        strokeWidth: { 5: 3 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 10: 180 },
        radius: 15
    },
    polygon3: {
        shape: 'polygon',
        points: 5,
        top: '90%',
        left: '10%',
        fill: 'none',
        stroke: 'rgba(0,255,255, 1)',
        strokeWidth: { 5: 3 },
        strokeDasharray: '100%',
        strokeDashoffset: { '100%': '-100%' },
        angle: { 10: 180 },
        radius: 15
    },
    rect1: {
        shape: 'rect',
        top: '25%',
        left: '35%',
        fill: 'none',
        radius: 12,
        stroke: { '#02b5c1': 'rgba(0,255,255, 1)' },
        strokeWidth: { 8: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 100: 280 }
    },
    rect2: {
        shape: 'rect',
        top: '80%',
        left: '60%',
        fill: 'none',
        radius: 15,
        stroke: { 'magenta': '#c7687e' },
        strokeWidth: { 8: 1 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 90: 0 }
    }
};

var figuresDesktop = {
    circle1: {
        shape: 'circle',
        top: '25%',
        left: '78%',
        fill: 'none',
        radius: 3,
        stroke: 'rgba(0,0,0, 1)',
        strokeWidth: { 4: 0 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' }
    },
    circle2: {
        shape: 'circle',
        top: {'1%': '99%'},
        left: {'1%': '99%'},
        fill: '#000',
        radius: 2,
        stroke: 'rgba(0,0,0, 1)',
        strokeWidth: 1,

        duration: 10000
    },
    circle3: {
        shape: 'circle',
        top: {'99%': '21%'},
        left: {'81%': '10%'},
        fill: '#000',
        radius: 2,
        stroke: 'rgba(0,0,0, 1)',
        strokeWidth: 1,

        duration: 12000
    },
    circle4: {
        shape: 'circle',
        top: {'1%': '99%'},
        left: {'61%': '1%'},
        fill: '#000',
        radius: 2,
        stroke: 'rgba(0,0,0, 1)',
        strokeWidth: 1,

        duration: 8000
    },
    circle5: {
        shape: 'circle',
        top: {'99%': '1%'},
        left: {'41%': '80%'},
        fill: '#000',
        radius: 2,
        stroke: 'rgba(0,0,0, 1)',
        strokeWidth: 1,

        duration: 15000
    },
    zigzag1: {
        shape: 'zigzag',
        top: '70%',
        left: '78%',
        fill: 'none',
        radius: 45,
        stroke: { 'rgba(0,255,255, 1)': 'magenta' },
        strokeWidth: { 3: 1 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 180: 90 }
    },
    zigzag2: {
        shape: 'zigzag',
        top: '75%',
        left: '28%',
        fill: 'none',
        radius: 45,
        stroke: { 'rgba(0,255,255, 1)': 'magenta' },
        strokeWidth: { 3: 1 },
        strokeDasharray: '100%',
        strokeDashoffset: { '-100%': '100%' },
        angle: { 0: -90 }
    }
}


function drawBackgroundElement(element, timeIncrement) {
    setTimeout(function () {
        var draw = new mojs.Shape(element).play();
    }, timeIncrement * 50);
}

function drawMobileBackground() {
    var timeIncrement = 10;
    for (var key in figuresMobile) {
        var element = figuresMobile[key];
        element.parent = 'div.container-fluid.main';
        element.duration = 600*timeIncrement;
        element.repeat = 999;
        drawBackgroundElement(element, timeIncrement);
        timeIncrement++;
    }
}

function drawDesktopBackground() {
    var timeIncrement = 10;
    for (var key in figuresDesktop) {
        var element = figuresDesktop[key];
        element.parent = 'div.container-fluid.main';
        element.duration = element.duration || 600*timeIncrement;
        element.repeat = 999;
        drawBackgroundElement(element, timeIncrement);
        timeIncrement++;
    }
}

function drawBackground() {
    drawMobileBackground();
    if (screen.width >= 992) {
        drawDesktopBackground();
    }
}

if (location.pathname == '/') {
    drawBackground();
}
Snap.plugin(function (Snap, Element, Paper, global) {
    function polarToCartesian(centerX, centerY, radius, angleInDegrees) {
        var angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0;

        return {
            x: centerX + (radius * Math.cos(angleInRadians)),
            y: centerY + (radius * Math.sin(angleInRadians))
        };
    }

    Paper.prototype.arcPath = function (x, y, radius, startAngle, endAngle, revert) {
        if (revert) {
            radius += 5;
        }

        var start = polarToCartesian(x, y, radius, endAngle),
            end = polarToCartesian(x, y, radius, startAngle),
            largeArc = endAngle - startAngle <= 180 ? "1" : "0";

        var align = 1;

        if (revert) {
            largeArc = largeArc == "1" ? "0" : "1";
            align = 0;
        }

        var path = "M" + start.x + "," + start.y + " A" + radius + "," + radius + " 0 " + largeArc + "," + align + " " + end.x + "," + end.y;

        return this.path(path);

    };
});

var canvasSize = 100,
    centre = canvasSize / 2,
    radius = canvasSize / 2.3,
    s = Snap("#radial-menu-svg"),
    startY = centre

function getTextArcId(name) {
    return name + 'arc';
}

function activateMenuItem(text) {
    var arcId = text.data('arc-id');
    var slideId = text.data('slide-id');
    var degres = text.data('degres');
    var textArc = arcId && $('#' + arcId);
    var isRadialItem = textArc != null;
    var slide = $('#' + slideId);

    $('.radial-menu text.active').removeClass('active');
    $('.radial-menu path.active-arc-item').removeClass('active-arc-item');
    $('.radial-menu-slide.active').removeClass('active');

    text.addClass('active');
    slide.addClass('active');

    if (isRadialItem) {
        textArc.addClass('active-arc-item');
        rotateMenuBorderArcs(degres);
        if ($('.radar-items path.transparent').length) {
            toggleMenuBorderArcs();
        }
    } else if ($('circle.transparent').length) {
        toggleMenuBorderArcs();
    }
}

function drawMenuItem(menuItem) {
    var arcId = getTextArcId(menuItem.textParams.text);
    var arc = s.arcPath(startY, startY, radius - 5, menuItem.arcParams.startAngle, menuItem.arcParams.endAngle).attr({
        id: arcId
    });

    var label = menuItem.textParams.text && menuItem.textParams.text.toUpperCase();
    var textPath = s.arcPath(startY, startY, radius, menuItem.textParams.startAngle, menuItem.textParams.endAngle, menuItem.textParams.revert).attr({
        id: menuItem.textParams.text,
        stroke: 'transparent',
        strokeWidth: 0
    });

    var text = s.text(0, 0, label).attr({
        'textpath': textPath
    });

    text.data('arc-id', arcId);
    text.data('slide-id', menuItem.slideParams.id);
    text.data('degres', menuItem.textParams.degres);

    text.click(function () {
        activateMenuItem(this);
    });

    return text;
}

function drawCenterMenuItem(menuItem) {
    var label = menuItem.textParams.text && menuItem.textParams.text;
    
    var text = s.text(menuItem.textParams.x, menuItem.textParams.y, label).addClass('center-item');
    text.data('slide-id', menuItem.slideParams.id);

    text.click(function () {
        activateMenuItem(this);
    });

    return text;
}

function drawMenuBorderArcs() {
    // Сектор
    var section = s.arcPath(startY, startY, radius - 20, -5, 270).addClass('section-1');
    // Дуга
    var arc = s.arcPath(startY, startY, radius - 20, 260, 360).addClass('arc-0');
    // Группа
    var discs = s.group(section, arc).addClass('radar-items');
    var circle = s.circle(startY, startY, radius - 20).addClass('circle transparent');
}

function toggleMenuBorderArcs() {
    $('.radar-items path').toggleClass('transparent');
    $('circle').toggleClass('transparent');
}

function rotateMenuBorderArcs(degres) {
    var rotateParams = 'rotate(' + degres + ' ' + startY + ' ' + startY + ')';
    var section = s.select('.radar-items path.section-1');
    var arc = s.select('.radar-items path.arc-0');

    if (section && arc) {
        section.transform(rotateParams);
        arc.transform(rotateParams);
    }
}

function drawMenu() {
    if (!s) {
        return;
    }

    var menuItems = [{
        textParams: {
            startAngle: -35,
            endAngle: 280,
            degres: 0,
            text: 'mobile',
            revert: false
        },
        arcParams: {
            startAngle: -15,
            endAngle: 260
        },
        slideParams: {
            id: 'slide1'
        }
    }, {
        textParams: {
            startAngle: 65,
            endAngle: 360,
            degres: 90,
            text: 'back-end',
            revert: false,
        },
        arcParams: {
            startAngle: 75,
            endAngle: 350
        },
        slideParams: {
            id: 'slide2'
        }
    }, {
        textParams: {
            startAngle: 120,
            endAngle: 135,
            degres: 180,
            text: 'qa',
            revert: true
        },
        arcParams: {
            startAngle: -190,
            endAngle: 80
        },
        slideParams: {
            id: 'slide3'
        }
    }, {
        textParams: {
            startAngle: 200,
            endAngle: 225,
            degres: 270,
            text: 'web',
            revert: true
        },
        arcParams: {
            startAngle: -105,
            endAngle: 175
        },
        slideParams: {
            id: 'slide4'
        }
    }];

    var centerMenuItem = {
        textParams: {
            text: 'DevOps',
            x: (canvasSize / 2) - 4,
            y: (canvasSize / 2) - 6
        },
        slideParams: {
            id: 'slide5'
        }
    };

    for (var i = 0; i < menuItems.length; i++) {
        var menuItem = menuItems[i];
        var text = drawMenuItem(menuItem);
        if (i == 0) {
            activateMenuItem(text);
        }
    }

    drawCenterMenuItem(centerMenuItem);
    drawMenuBorderArcs();
}

drawMenu();