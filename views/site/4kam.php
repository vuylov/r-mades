<?php
/** @var $this \yii\web\View */

$this->title = '4kam';
?>
<div class="container-fluid main">
    <header class="main-header light-blue">
        <div class="logo-title content-indent">
            <h3 class="text-uppercase"><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                    class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                            d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                            id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                            d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                            id="bottom"
                            transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <hr>
        <div class="header-logo-container text-center text-uppercase"><a class="header-logo" href="/"><img
                    src="/assets/context/ready-mades-01-7.png"></a></div>
        <div class="row">
            <div class="col-sm-12 product-logo-container"><img class="img-responsive center-block"
                                                               src="/assets/context/products/4kam/header-logo.png"></div>
        </div>
    </header>
    <div class="main-content">
        <section class="description-section content-indent">
            <div class="row">
                <div class="col-sm-12"><p class="core-sans-a35-light text-center">Модный и технологичный продукт,
                        который помогает менеджерам по работе с ключевыми клиентами Henkel планировать объемы отгрузок и
                        промо-кампании, контролировать листинг, мгновенно получать информацию по нужным показателям.
                        Приоритеты заказчика — создание удобной и быстрой программы, понятной даже новичку в сегменте
                        FMCG.</p></div>
            </div>
            <div class="row screenshots-row">
                <div class="img-responsive center-block screenshot-left"
                     style="height: 350px; background-color: #35556b"></div>
                <div class="img-responsive center-block screenshot-right"
                     style="height: 350px; background-color: #8cced5"></div>
            </div>
        </section>
        <section class="content-indent">
            <div class="section-header-container text-center"><h1 class="section-header text-uppercase">Функционал</h1>
            </div>
            <p class="core-sans-a35-light text-center">Сотрудник Henkel гибко управляет сетями, выставляя объемы
                отгружаемой продукции по каждой позиции каталога, держит под контролем мгновенно расчитываемый
                показатель PC, заводит и согласовывает промо-кампании, планирует развитие листинга на периоде и
                проверяет листинг по продажам. В 4KAM реализована ролевая структура, благодаря которой руководство в HQ
                имеет возможность утверждать кампании, ставить цели по показателям и KPI и управлять ассортиментом.</p>
        </section>
        <section class="screenshots-section kam content-indent">
            <div class="screenshots-container single"><img class="img-responsive"
                                                           src="/assets/context/products/4kam/screen-1.png"></div>
        </section>
        <section class="screenshots-section kam-2 content-indent">
            <div class="screenshots-tablet"><img class="img-responsive" src="/assets/context/products/4kam/screen-2.png">
            </div>
        </section>
        <section class="facts-section kam content-indent">
            <div class="facts-container">
                <div class="facts-header-container text-center"><h1 class="section-header text-uppercase">Факты</h1>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="200">>
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Менеджеров по работе с ключевыми<br>клиентамиработает в
                            программе</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="457">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Национальных и региональных<br>сетей,куда производятся<br>отгрузки
                            товаров Henkel</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="375">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Товарных позиций, по которым<br>проводится планирование
                        </p></div>
                </div>
            </div>
        </section>
        <?= $this->render('_footer');?>
    </div>
</div>
<?=$this->render('menu', ['style' => '']);?>