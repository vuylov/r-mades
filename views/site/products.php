<?php
/** @var $this \yii\web\View */

use yii\helpers\Html;
$this->title = 'Продукты';
?>
<div class="container-fluid main">
    <header class="main-header dark text-uppercase">
        <div class="logo-title content-indent">
            <h3><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                    class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                            d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                            id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                            d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                            id="bottom"
                            transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <hr>
        <div class="header-logo-container text-center"><a class="header-logo" href="/"><img
                    src="assets/context/ready-mades-alt-01-2.png"></a></div>
        <div class="row">
            <div class="col-sm-12 text-uppercase text-center"><h1 class="header-title">продукты</h1></div>
        </div>
    </header>
    <div class="main-content">
        <section class="products-section content-indent">
            <div class="products-container">
                <figure class="product"><a href="/products/equeo"><p><img src="assets/context/products/logos/e-queo.png">
                        </p>
                        <figcaption class="left-indent">iOS, Android, Web</figcaption>
                    </a></figure>
                <figure class="product"><a href="products/4kam"><p><img src="assets/context/products/logos/4kam.png"></p>
                        <figcaption class="left-indent dark">Web</figcaption>
                    </a></figure>
                <figure class="product"><a href="products/myteam"><p><img src="assets/context/products/logos/mmt.png"></p>
                        <figcaption class="left-indent">iOS, Android, Web</figcaption>
                    </a></figure>
                <figure class="product"><a href="products/takeda"><p><img src="assets/context/products/logos/takeda.png">
                        </p>
                        <figcaption class="left-indent">iOS, Android</figcaption>
                    </a></figure>
                <figure class="product"><a href="products/lafarge"><p><img
                                src="assets/context/products/logos/lafargeholcim.png"></p>
                        <figcaption class="left-indent">Web</figcaption>
                    </a></figure>
                <figure class="product"><p><img src="assets/context/products/logos/planogrammer.png"></p>
                    <figcaption class="text-center">Продукт<br>в разработке</figcaption>
                </figure>
            </div>
        </section>
        <footer class="main-footer content-indent">
            <div class="footer-logo-container text-center">
                <div class="footer-logo"><img src="assets/context/ready-mades-01-7.png"></div>
            </div>
            <div class="row">
                <div class="col-sm-6 footer-text footer-left-text"><h2 class="core-sans-a65-bold red">Всегда рады<br>общению!
                    </h2></div>
                <div class="col-sm-6 footer-text footer-right-text core-sans-a25-extra-light"><h2
                        class="footer-text-phone">+7 (969) 656 73 26</h2>
                    <h2 class="footer-text-email">welcomе@r-mades.com</h2>
                    <h3 class="address">400136, г.Волгоград, ул.&nbsp;Канунникова,<br>д.23, БЦ Дельта</h3>
                    <h3 class="address">107140, г. Москва, ул. Русаковская,<br>д.13, БЦ Бородино Плаза</h3></div>
            </div>
            <div class="row">
                <div class="col-sm-12 social"><img src="assets/context/social/google-plus.png"> <img
                        src="assets/context/social/vk.png"> <a href="https://www.facebook.com/readymadesmobile/"
                                                               target="_blank"><img
                            src="assets/context/social/facebook.png"></a></div>
            </div>
            <div class="copyright text-uppercase text-center"><img src="assets/context/coffee-man.png"><h5
                    class="core-sans-a35-light text-normal">© all rights reserved</h5></div>
        </footer>
    </div>
</div>
<?=$this->render('menu', ['style' => 'white']);?>