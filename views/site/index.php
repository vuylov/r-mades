<?php

/* @var $this yii\web\View */
/* @var $form \app\models\ContactForm */
use yii\helpers\Html;

$this->title = 'R-mades';
?>
<div class="container-fluid main">
    <div class="main-content-full">
        <div class="main-center-name content-indent">
            <h3 class="text-uppercase">центр&#13;&#10;мобильной<br>разработки <span
                        class="main-menu-toggle black visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                                d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                                id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                                d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                                id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <div class="main-logo"></div>
        <div class="main-navigation main-navigation-left text-uppercase">продукты</div>
        <div class="main-navigation main-navigation-right text-uppercase">о нас</div>
        <div class="main-navigation main-navigation-bottom text-uppercase"><a href="/development">разработка</a>
        </div>
    </div>
</div>
<?= $this->render('menu', ['style' => '']);?>