<?php
/* @var $this yii\web\View */
/* @var $form app\models\ContactForm */

use yii\helpers\Html;
use yii\helpers\Url;
?>
<?= Html::beginForm('/', 'POST', [
        'id' => 'contact-form',
        'class' => ''
]);?>
    <div class="col-xs-12 col-sm-8 col-md-4">
        <div class="hidden-xs splitter"></div>
        <?= Html::label('Имя', 'ContactForm[name]', ['class' => 'sr-only']);?>
        <?= Html::input('text', 'ContactForm[name]', null, [
            'id' => 'form-name',
            'class' => 'form-control',
            'placeholder' => 'имя'
        ]); ?>
    </div>
    <div class="col-xs-12 col-sm-offset-4 col-sm-8 col-md-offset-0 col-md-4 upper-top-sm">
        <?= Html::label('Телефон', 'ContactForm[phone]', ['class' => 'sr-only']);?>
        <?= Html::input('text', 'ContactForm[phone]', null, [
            'id' => 'form-phone',
            'class' => 'form-control',
            'placeholder' => 'телефон'
        ]); ?>
    </div>
    <div class="col-xs-12 col-sm-offset-4 col-sm-8 col-md-offset-0 col-md-4">
        <button type="submit" class="btn btn-primary"><span
                    class="visible-xs">Оставить заявку</span> <span class="hidden-xs">Отправить</span>
        </button>
    </div>
<?= Html::endForm();?>
<?php
$url = Url::to(['contact']);
$error = \Yii::$app->params['messages']['error'];
$js = <<< JS
    $(document).ready(function(){        
        $("#contact-form").on("submit", function(){
            var name = $("#form-name").val();
            var phone = $("#form-phone").val();
            
            if(name && phone){
                $.ajax({
                    url: "{$url}",
                    method: "POST",
                    data: $(this).serialize(),
                }).done(function(message){
                    iziToast.success({
                        title: 'Отправлено',
                        message: message,
                        position: 'center',
                        timeout:  5000,
                        layout: 2
                    });
                    
                }).fail(function(message) {
                    iziToast.error({
                        title: 'Ошибка',
                        message: "{$error}",
                        position: 'center'
                    });
                });
                
                $(".hide-contact-us").trigger('click');
                $(this).trigger('reset');
            }
            return false;
        });  
    });
JS;
$this->registerJs($js, \yii\web\View::POS_END);