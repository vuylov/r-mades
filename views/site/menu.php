<?php
use yii\helpers\Html;
?>
<nav class="main-menu content-indent">
    <div class="row text-uppercase">
        <div class="col-xs-12 col-sm-5 col-md-4 col-lg-5"><a class="menu-link" href="/"><img class="logo-img pull-left"
                                                                                             src="/assets/context/main-menu/r-m-menu.png">
                <h3 class="logo-text">центр<br>мобильной<br>разработки</h3></a>
            <div class="remove-contact-us"></div>
        </div>
        <div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-2">
                    <?= Html::a(Html::tag('h3', 'о нас', ['class' => 'menu-text']), ['/'], ['class' => 'menu-link']);?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-3">
                    <?= Html::a(Html::tag('h3', 'разработка', ['class' => 'menu-text']), ['/development'], ['class' => 'menu-link']);?>
                </div>
                <div class="col-xs-12 col-sm-4 col-md-2 text-right-sm">
                    <?= Html::a(Html::tag('h3', 'Продукты', ['class' => 'menu-text']), ['/products'], ['class' => 'menu-link']);?>
                </div>
                <div class="col-xs-12 col-sm-offset-4 col-sm-4 col-md-offset-1 col-md-2"><a
                            class="menu-link show-contact-us" href="#" data-target="send-request"><h3
                                class="menu-text indent-contact-link-sm">связь</h3></a></div>
                <div class="col-xs-12 col-sm-4 col-md-2 nopadding-left text-right-sm"><a
                            class="menu-link show-contact-us" href="#" data-target="contacts"><h3 class="menu-text">
                            контакты</h3></a></div>
            </div>
        </div>
    </div>
    <div class="contact-us-row send-request">
        <div class="row">
            <div class="hidden-xs col-sm-5 col-md-4 col-lg-5">
                <div class="social-container justify-content-between pull-left"><a href="http://www.e-queo.info/"
                                                                                   target="_blank"><img
                                src="/assets/context/main-menu/e-queo-header-menu-logo.png"> </a><a
                            href="https://www.facebook.com/readymadesmobile/" target="_blank"><img
                                src="/assets/context/main-menu/fb.png"></a></div>
                <p class="hidden-xs hidden-sm core-sans-a65-bold">Оставьте заявку.<br>Мы свяжемся&#13;&#10;с Вами!</p>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 nopadding hidden-md hidden-lg">
                        <div class="move-left"><p class="contact-us-text core-sans-a65-bold">Оставьте заявку.<br>Мы
                                свяжемся&#13;&#10;с Вами!</p></div>
                    </div>
                    <?= $this->render('contact');?>
                </div>
            </div>
        </div>
    </div>
    <div class="contact-us-row contacts">
        <div class="row">
            <div class="hidden-xs col-sm-5 col-md-4 col-
lg-5">
                <div class="social-container justify-content-between pull-left"><a href="http://www.e-queo.info/"
                                                                                   target="_blank"><img
                                class="header-logos-item" src="/assets/context/main-menu/e-queo-header-menu-logo.png">
                    </a><a href="https://www.facebook.com/readymadesmobile/" target="_blank"><img
                                class="header-logos-item" src="/assets/context/main-menu/fb.png"></a></div>
                <p class="hidden-xs hidden-sm core-sans-a65-bold">Оставьте заявку.<br>Мы свяжемся&#13;&#10;с Вами!</p>
            </div>
            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-7">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 nopadding hidden-md hidden-lg">
                        <div class="move-left"><p class="contact-us-text core-sans-a65-bold">Свяжитесь с нами.<br>Мы
                                всегда рады&#13;&#10;общению!</p></div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-5 justify-content-between">
                        <div class="hidden-xs splitter"></div>
                        <div class="visible-xs"></div>
                        <div class="contacts-container"><p class="core-sans-a65-bold email">welcomе@r-mades.com</p><h4
                                    class="core-sans-a35-light phone blue">+7 (969) 656 73 26</h4></div>
                        <div></div>
                    </div>
                    <div class="col-xs-12 col-sm-offset-4 col-sm-8 col-md-offset-0 col-md-6 justify-content-between upper-top-sm">
                        <div class="splitter hidden-xs hidden-sm"></div>
                        <div class="visible-xs visible-sm"></div>
                        <div><p class="address visible-xs">400136, г.Волгоград,<br>ул. Канунникова, д.23</p>
                            <p class="address visible-sm">400136, г.Волгоград, ул. Канунникова,<br>д.23, БЦ Дельта</p>
                            <p class="address hidden-xs hidden-sm">400136, г.Волгоград, ул. Канунникова, д.23,<br>БЦ
                                Дельта</p>
                            <p class="address visible-xs">107140, г. Москва,<br>ул. Русаковская, д.13</p>
                            <p class="address visible-sm">107140, г. Москва, ул. Русаковская,<br>д.13, БЦ Бородино Плаза
                            </p>
                            <p class="address hidden-xs hidden-sm">107140, г. Москва, ул. Русаковская, д.13,<br>БЦ
                                Бородино Плаза</p></div>
                        <div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row social-container-row-xs">
        <div class="col-xs-12">
            <div class="social-container-xs justify-content-between"><a href="http://www.e-queo.info/"
                                                                        target="_blank"><img class="header-logos-item"
                                                                                             src="/assets/context/main-menu/e-queo-header-menu-logo.png">
                </a><a href="https://www.facebook.com/readymadesmobile/" target="_blank"><img class="header-logos-item"
                                                                                              src="/assets/context/main-menu/fb.png"></a>
            </div>
        </div>
    </div>
    <div class="menu-badge <?=$style;?> text-uppercase"><h3 class="menu-text">меню</h3>
        <h3 class="hide-contact-us">&times;</h3></div>
</nav>