<footer class="main-footer content-indent">
    <div class="footer-logo-container text-center">
        <div class="footer-logo"><img src="/assets/context/ready-mades-01-7.png"></div>
    </div>
    <div class="row">
        <div class="col-sm-6 footer-text footer-left-text"><h2 class="core-sans-a65-bold red">Всегда рады<br>общению!
            </h2></div>
        <div class="col-sm-6 footer-text footer-right-text core-sans-a25-extra-light"><h2
                class="footer-text-phone">+7 (969) 656 73 26</h2>
            <h2 class="footer-text-email">welcomе@r-mades.com</h2>
            <h3 class="address">400136, г.Волгоград, ул.&nbsp;Канунникова,<br>д.23, БЦ Дельта</h3>
            <h3 class="address">107140, г. Москва, ул. Русаковская,<br>д.13, БЦ Бородино Плаза</h3></div>
    </div>
    <div class="row">
        <div class="col-sm-12 social"><img src="/assets/context/social/google-plus.png"> <img
                src="/assets/context/social/vk.png"> <a href="https://www.facebook.com/readymadesmobile/"
                                                       target="_blank"><img
                    src="/assets/context/social/facebook.png"></a></div>
    </div>
    <div class="copyright text-uppercase text-center"><img src="/assets/context/coffee-man.png"><h5
            class="core-sans-a35-light text-normal">© all rights reserved</h5></div>
</footer>