<?php
/** @var $this \yii\web\View */

$this->title = 'MyTeam';
?>
<div class="container-fluid main">
    <header class="main-header green">
        <div class="logo-title content-indent">
            <h3 class="text-uppercase"><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                    class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                            d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                            id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                            d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                            id="bottom"
                            transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <hr>
        <div class="header-logo-container text-center text-uppercase"><a class="header-logo" href="/"><img
                    src="/assets/context/ready-mades-01-2.png"></a></div>
        <div class="row">
            <div class="col-sm-12 product-logo-container"><img class="img-responsive center-block"
                                                               src="/assets/context/products/mmt/header-logo.png"></div>
        </div>
    </header>
    <div class="main-content">
        <section class="description-section mmt content-indent">
            <div class="row">
                <div class="col-sm-12"><p class="core-sans-a35-light text-center">Продукт предназначен для&nbsp;обучения
                        персонала стандартам работы во&nbsp;время рабочих визитов и&nbsp;контроля активности сотрудников
                        на&nbsp;мобильных устройствах. Идеальный инструмент для начальников, полевых супервайзеров и&nbsp;их&nbsp;команд.
                        На&nbsp;данный момент решение уже внедрено в&nbsp;нескольких крупных FMCG компаниях.</p></div>
            </div>
            <div class="row screenshots-row"><img class="img-responsive center-block screenshot-left"
                                                  src="/assets/context/products/mmt/product-screen-left.png"> <img
                    class="img-responsive center-block screenshot-right"
                    src="/assets/context/products/mmt/product-screen-right.png"></div>
        </section>
        <section class="content-indent">
            <div class="section-header-container text-center"><h1 class="section-header text-uppercase">Функционал</h1>
            </div>
            <p class="core-sans-a35-light text-center">Пользователи программы — полевые начальники и&nbsp;полевые
                сотрудники. Первые планируют активности команды — от&nbsp;индивидуальных встреч до&nbsp;коллективных
                собраний. Вторые выполняют тренинги и&nbsp;задачи. Программа отслеживает их&nbsp;перемещения по&nbsp;торговым
                точкам и&nbsp;активность в&nbsp;течение рабочего дня. Реализованы интерактивные отчеты для
                полевыхначальников и&nbsp;менеджеров территории.</p></section>
        <section class="screenshots-section mmt content-indent">
            <div class="screenshots-tablet"><img class="img-responsive" src="/assets/context/products/mmt/tablet-1.png">
            </div>
        </section>
        <section class="screenshots-section mmt-2 content-indent">
            <div class="screenshots-tablet"><img class="img-responsive" src="/assets/context/products/mmt/tablet-2.png">
            </div>
        </section>
        <section class="facts-section mmt content-indent">
            <div class="facts-container">
                <div class="facts-header-container text-center"><h1 class="section-header text-uppercase">Факты</h1>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="1000">>
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Полевых начальников и полевых<br>сотрудников
                            используют<br>My Team в работе</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="5000">~
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Тренингов проводится с помощью<br>нашей программы
                            ежемесячно</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="9000">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Фотографий за месяц отправляется<br>полевым начальникам в
                            рамках исполнения<br>задач по развитию территории</p></div>
                </div>
            </div>
        </section>
        <?= $this->render('_footer');?>
    </div>
</div>
<?=$this->render('menu', ['style' => '']);?>