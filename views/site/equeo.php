<?php
/** @var $this \yii\web\View */

$this->title = 'equeo';
?>
<div class="container-fluid main">
    <header class="main-header dark-blue">
        <div class="logo-title content-indent">
            <h3 class="text-uppercase"><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                    class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                            d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                            id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                            d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                            id="bottom"
                            transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <hr>
        <div class="header-logo-container text-center text-uppercase"><a class="header-logo" href="/"><img
                    src="/assets/context/ready-mades-alt-01-2.png"></a></div>
        <div class="row">
            <div class="col-sm-12 product-logo-container"><img class="img-responsive center-block"
                                                               src="/assets/context/products/equeo/header-logo.png">
                <p class="core-sans-a35-light text-center">Платформа для обучения и бизнес-коммуникации e.Queo</p></div>
        </div>
    </header>
    <div class="main-content">
        <section class="description-section content-indent">
            <div class="row">
                <div class="col-sm-12"><p class="core-sans-a35-light text-center">Основная идея e.Queo — перенос
                        традиционного обучения сотрудников в цифровую среду. Персонал должен иметь быстрый и удобный
                        доступ к обучающим материалам, онлайн-аттестациям и статистике по их прохождению. Упор делается
                        на видео-контент премиум качества и нативные мобильные приложения для iOS и Android.</p></div>
            </div>
            <div class="row screenshots-row">
                <div class="img-responsive center-block screenshot-left"
                     style="height: 350px; background-color: #35556b"></div>
                <div class="img-responsive center-block screenshot-right"
                     style="height: 350px; background-color: #8cced5"></div>
            </div>
        </section>
        <section class="content-indent">
            <div class="section-header-container text-center"><h1 class="section-header text-uppercase">Функционал</h1>
            </div>
            <p class="core-sans-a35-light text-center">«Учись» — каталог профессионально созданных видеотренингов с
                финальными тестами. «Узнавай» — лента видео для микро-обучения и тренировки soft skills. «Аттестация» —
                проверка знаний и опросы. Также пользователю доступен раздел с дополнительной информацией (каталоги,
                памятки, планограммы) для эффективной работы вне офиса.</p></section>
        <section class="screenshots-section equeo content-indent">
            <div class="screenshots-container"><img class="screenshot" src="/assets/context/products/equeo/phone-1.png">
                <img class="screenshot" src="/assets/context/products/equeo/phone-2.png"> <img class="screenshot"
                                                                                              src="/assets/context/products/equeo/phone-1.png">
            </div>
        </section>
        <section class="screenshots-section equeo-2 content-indent">
            <div class="screenshots-tablet"><img class="img-responsive"
                                                 src="/assets/context/products/equeo/tablet-1.png"></div>
        </section>
        <section class="facts-section equeo content-indent">
            <div class="facts-container">
                <div class="facts-header-container text-center"><h1 class="section-header text-uppercase">Факты</h1>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="2.5">>
                            <span></span>K</h1>
                        <p class="core-sans-a55-medium small">Сотрудников в сфере<br>FMCG используют наше решение</p>
                    </div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="20">
                            <span></span>+</h1>
                        <p class="core-sans-a55-medium small">Человек обеспечивают<br>техническую поддержку<br>пользователей
                            в рабочее время</p></div>
                    <div class="col-sm-4 text-center">
                        <h1 class="core-sans-a25-extra-light big red" data-grow="7"><span></span>
                            <small class="red text-uppercase">new</small>
                        </h1>
                        <p class="core-sans-a55-medium small">Глобальных
                            обновлений<br>приложения мы осуществили<br>с 2015 года</p></div>
                </div>
            </div>
        </section>
        <?= $this->render('_footer');?>
    </div>
</div>
<?=$this->render('menu', ['style' => '']);?>