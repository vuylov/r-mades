<?php
/** @var $this \yii\web\View */

$this->title = 'Takeda';
?>
<div class="container-fluid main">
    <header class="main-header dark-2">
        <div class="logo-title content-indent">
            <h3 class="text-uppercase"><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                    class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                            d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                            id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                            d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                            id="bottom"
                            transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
            </h3>
        </div>
        <hr>
        <div class="header-logo-container text-center text-uppercase"><a class="header-logo" href="/"><img
                    src="/assets/context/ready-mades-alt-01-2.png"></a></div>
        <div class="row">
            <div class="col-sm-12 product-logo-container"><img class="img-responsive center-block"
                                                               src="/assets/context/products/takeda/header-logo.png">
            </div>
        </div>
    </header>
    <div class="main-content">
        <section class="description-section takeda content-indent">
            <div class="row">
                <div class="col-sm-12"><p class="core-sans-a35-light text-center">Продукт предназначен для проведения
                        практических упражнений, тренингов, центров оценки персонала, коучинг сессий с помощью мобильных
                        устройств.</p></div>
            </div>
            <div class="row screenshots-row"><img class="img-responsive center-block screenshot-left"
                                                  src="/assets/context/products/takeda/product-screen-left.png"> <img
                    class="img-responsive center-block screenshot-right"
                    src="/assets/context/products/takeda/product-screen-right.png"></div>
        </section>
        <section class="content-indent">
            <div class="section-header-container text-center"><h1 class="section-header text-uppercase">Функционал</h1>
            </div>
            <p class="core-sans-a35-light text-center">Пользователи программы — рядовые сотрудники, которым необходимо
                оттачить навыки презентации и продажи продукта. Сотрудники делятся на команды по 3-5 человек. В каждой
                команаде определеяется роль, которую сотрудник будет отыгрывать в данной сессии. По итогам сессии
                наблюдателем, из числа фасилитаторов выставляется оценка каждому участнику сессии. Данные формируются в
                удобную форму отчета.</p></section>
        <section class="screenshots-section takeda content-indent">
            <div class="screenshots-container single"><img class="img-responsive"
                                                           src="/assets/context/products/takeda/tablet-1.png"></div>
        </section>
        <section class="screenshots-section takeda-2 content-indent">
            <div class="screenshots-tablet"><img class="img-responsive center-block"
                                                 src="/assets/context/products/takeda/tablet-2.png"> <img
                    class="img-responsive center-block" src="/assets/context/products/takeda/tablet-3.png"></div>
        </section>
        <section class="facts-section takeda content-indent">
            <div class="facts-container-takeda">
                <div class="facts-header-container text-center"><h1 class="section-header text-uppercase">Факты</h1>
                </div>
                <div class="row">
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="1000">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Cотрудников пользуются<br>удобным инструментом<br>игровых
                            сессий</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="4">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Массовых тренинга<br>проводится с помощью<br>нашей
                            программы ежегодно</p></div>
                    <div class="col-sm-4 text-center"><h1 class="core-sans-a25-extra-light big red" data-grow="20">
                            <span></span></h1>
                        <p class="core-sans-a55-medium small">Массовых тренинга<br>проводится с помощью<br>нашей
                            программы ежегодно</p></div>
                </div>
            </div>
        </section>
        <?= $this->render('_footer');?>
    </div>
</div>
<?=$this->render('menu', ['style' => 'white']);?>