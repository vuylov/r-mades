<?php
/** @var $this \yii\web\View */

use yii\helpers\Html;
$this->title = 'Разработка';
?>
    <div class="container-fluid main">
        <header class="main-header text-uppercase">
            <div class="logo-title content-indent">
                <h3><a href="/">центр&#13;&#10;мобильной<br>разработки </a><span
                            class="main-menu-toggle visible-xs pull-right"><svg viewBox="0 0 800 600"><path
                                    d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200"
                                    id="top"></path><path d="M300,320 L540,320" id="middle"></path><path
                                    d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190"
                                    id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path></svg></span>
                </h3>
            </div>
            <hr>
            <div class="header-logo-container text-center"><a class="header-logo" href="/"><img
                            src="assets/context/ready-mades-01-2.png"></a></div>
            <div class="row">
                <div class="col-sm-12 text-uppercase text-center"><h1 class="header-title">разработка</h1></div>
            </div>
        </header>
        <div class="main-content">
            <section class="content-indent">
                <div class="row">
                    <div class="col-sm-12"><p class="core-sans-a35-light text-center">Мы всегда думаем о долгосрочном
                            развитии программного продукта, именно поэтому тщательно изучаем специфику вашего бизнеса. Нам
                            интересно и проводить масштабную автоматизацию и создавать простые приложения-визитки. Готовы
                            подключиться к&nbsp;проекту на любом этапе: довести до ума работы на финальной стадии или&nbsp;воплотить&nbsp;вашу
                            идею в функциональных требованиях в самом начале.</p></div>
                </div>
            </section>
            <section class="content-indent">
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-2 justify-content-around">
                        <figure class="development"><p><img src="../assets/context/development/ui-ux.png"></p>
                            <figcaption class="red">UI/UX проектирование и дизайн</figcaption>
                        </figure>
                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-10">
                        <div class="dev-container dev-border">
                            <div class="col-xs-12 col-sm-6 justify-content-around">
                                <figure class="development"><p><img src="../assets/context/development/web.png"></p>
                                    <figcaption class="blue">WEB</figcaption>
                                </figure>
                                <figure class="development"><p><img src="../assets/context/development/apple.png"></p>
                                    <figcaption class="blue">iOS</figcaption>
                                </figure>
                            </div>
                            <div class="col-xs-12 col-sm-6 justify-content-around">
                                <figure class="development"><p><img src="../assets/context/development/android.png"></p>
                                    <figcaption class="blue">Android</figcaption>
                                </figure>
                                <figure class="development"><p><img src="../assets/context/development/back-end.png"></p>
                                    <figcaption class="blue">Back-End</figcaption>
                                </figure>
                            </div>
                            <div class="dev-border-header-container"><span class="dev-border-header">Разработка</span></div>
                        </div>
                    </div>
                </div>
                <div class="row hidden-xs hidden-sm">
                    <div class="col-md-2 justify-content-around">
                        <figure class="development"><p><img src="../assets/context/development/qa.png"></p>
                            <figcaption class="red">Тестирование</figcaption>
                        </figure>
                    </div>
                    <div class="col-md-10">
                        <div class="col-xs-12 col-sm-6 justify-content-around">
                            <figure class="development"><p><img src="../assets/context/development/support.png"></p>
                                <figcaption class="red">Техническая поддержка</figcaption>
                            </figure>
                            <figure class="development"><p><img src="../assets/context/development/development.png"></p>
                                <figcaption class="red">Продвижение и развитие</figcaption>
                            </figure>
                        </div>
                        <div class="hidden-xs hidden-sm col-md-6 justify-content-around justify-content-center-sm">
                            <figure class="development"><p><img src="../assets/context/development/auditing.png"></p>
                                <figcaption class="red">Аудит<br>сайтов и приложений</figcaption>
                            </figure>
                            <figure class="development"><p><img src="../assets/context/development/services.png"></p>
                                <figcaption class="red">Услуги интеграции</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
                <div class="row hidden-md hidden-lg">
                    <div class="col-xs-12 col-sm-12"
                    >
                        <div>
                            <div class="col-xs-12 col-sm-6 justify-content-around">
                                <figure class="development"><p><img src="../assets/context/development/ui-ux.png"></p>
                                    <figcaption class="red">UI/UX<br>дизайн</figcaption>
                                </figure>
                                <figure class="development"><p><img src="../assets/context/development/qa.png"></p>
                                    <figcaption class="red">Тестирование</figcaption>
                                </figure>
                            </div>
                            <div class="col-xs-12 col-sm-6 justify-content-around">
                                <figure class="development"><p><img src="../assets/context/development/support.png"></p>
                                    <figcaption class="red">Техническая поддержка</figcaption>
                                </figure>
                                <figure class="development"><p><img src="../assets/context/development/development.png"></p>
                                    <figcaption class="red">Продвижение и развитие</figcaption>
                                </figure>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-12 justify-content-around">
                        <div class="col-xs-12 col-sm-6 justify-content-around">
                            <figure class="development"><p><img src="../assets/context/development/auditing.png"></p>
                                <figcaption class="red">Аудит<br>сайтов и приложений</figcaption>
                            </figure>
                            <figure class="development"><p><img src="../assets/context/development/services.png"></p>
                                <figcaption class="red">Услуги интеграции</figcaption>
                            </figure>
                        </div>
                    </div>
                </div>
            </section>
            <section class="dev-radial-menu content-indent">
                <div class="content-logo"><img src="assets/context/ready-mades-01-7.png"></div>
                <div class="text-center"><h1 class="section-header dev-header text-uppercase">технологии</h1></div>
                <p class="core-sans-a35-light text-center" style="margin-top: 10px; margin-bottom: 45px">Работаем и по
                    Waterfall и по Agile — в зависимости от специфики проекта. В&nbsp;обоих&nbsp;случаях обеспечиваем
                    непрерывный цикл разработки, тестирования и&nbsp;внедрения. На каждом из этапов применяем лучшие
                    практики и&nbsp;релевантные&nbsp;технологии.</p>
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="radial-menu">
                            <svg id="radial-menu-svg" viewBox="0 0 100 100"></svg>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="radial-menu-slides">
                            <div class="radial-menu-slide" id="slide1"><h3 class="core-sans-a65-bold text-uppercase">
                                    mobile</h3>
                                <hr>
                                <p class="core-sans-a55-medium small description">Пишем на основных языках программирования
                                    для iOS и&nbsp;Android. Используем актуальные фреймворки и&nbsp;библиотеки.</p>
                                <p class="core-sans-a65-bold red">Swift<br>Objective-C<br>Java</p></div>
                            <div class="radial-menu-slide" id="slide2"><h3 class="core-sans-a65-bold text-uppercase">
                                    back-end</h3>
                                <hr>
                                <p class="core-sans-a55-medium small description">Разрабатываем надежную и быструю серверную
                                    часть. Настраиваем систему управления базами данных. Подключаем сторонние сервисы.
                                    Создаем «облачную» инфраструктуру.</p>
                                <div class="row">
                                    <div class="col-xs-6"><p class="core-sans-a65-bold red">ASP.NET Core<br>Java<br>PHP<br>Python<br>
                                        </p></div>
                                    <div class="col-xs-6"><p class="core-sans-a65-bold red">MS SQL Server<br>PostgreSQL<br>MySQL<br>MongoDB
                                        </p></div>
                                </div>
                            </div>
                            <div class="radial-menu-slide" id="slide3"><h3 class="core-sans-a65-bold text-uppercase">qa</h3>
                                <hr>
                                <p class="core-sans-a55-medium small description">Проводим мануальное тестирование на
                                    соответствие софта техническому заданию. Повышаем скорость поиска багов с помощью
                                    автотестов. Для крупных клиентов с большой аудиторией регулярно делаем стресс-тесты
                                    перед релизом.</p>
                                <div class="row">
                                    <div class="col-xs-6"><p class="core-sans-a65-bold red">Appium<br>Selentium<br>Junit<br>
                                        </p></div>
                                    <div class="col-xs-6"><p class="core-sans-a65-bold red">JMeter<br>Java<br></p></div>
                                </div>
                            </div>
                            <div class="radial-menu-slide" id="slide4"><h3 class="core-sans-a65-bold text-uppercase">
                                    web</h3>
                                <hr>
                                <p class="core-sans-a55-medium small description">Создаем web-приложения, сайты и
                                    корпоративные порталы. Для разных задач используем разные типы фреймворков. Настраиваем
                                    понятную для вас систему управления контентом через панель администратора.</p>
                                <p class="core-sans-a65-bold red">HTML5<br>Angular<br>Yii2<br>CSS3<br></p></div>
                            <div class="radial-menu-slide" id="slide5"><h3 class="core-sans-a65-bold text-uppercase">
                                    DevOps</h3>
                                <hr>
                                <p class="core-sans-a55-medium small description">DevOps — это набор практик, которые
                                    позволяют интегрировать разработку, тестирование и эксплуатацию приложения или сайта в
                                    один непрерывный процесс. Благодаря этому наши клиенты получают обновления быстрее.</p>
                                <p class="core-sans-a65-bold red">Docker<br>Jenkins<br>Kubernetes<br>Rocket<br></p></div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="dev-clients content-indent text-center"><h1
                        class="section-header clients-header text-uppercase">Клиенты</h1><img
                        src="../assets/context/clients/henkel-logo-png.png">
                <div class="clients-list"><img src="../assets/context/clients/megapolis.png"> <img
                            src="../assets/context/clients/smollan-logo.png"> <img
                            src="../assets/context/clients/takeda.png"> <img
                            src="../assets/context/clients/logo-imperial-tobacco.png"> <img
                            src="../assets/context/clients/logo-loreal.png">
                    <div class="line-break"></div>
                    <img src="../assets/context/clients/fm.png"> <img src="../assets/context/clients/baltika.png"> <img
                            src="../assets/context/clients/lafarge.png"> <img src="../assets/context/clients/jde.png"> <img
                            src="../assets/context/clients/logo-pepsico.png"></div>
            </section>
            <footer class="main-footer content-indent">
                <div class="footer-logo-container text-center">
                    <div class="footer-logo"><img src="assets/context/ready-mades-01-7.png"></div>
                </div>
                <div class="row">
                    <div class="col-sm-6 footer-text footer-left-text"><h2 class="core-sans-a65-bold red">Всегда рады<br>общению!
                        </h2></div>
                    <div class="col-sm-6 footer-text footer-right-text core-sans-a25-extra-light"><h2
                                class="footer-text-phone">+7 (969) 656 73 26</h2>
                        <h2 class="footer-text-email">welcomе@r-mades.com</h2>
                        <h3 class="address">400136, г.Волгоград, ул.&nbsp;Канунникова,<br>д.23, БЦ Дельта</h3>
                        <h3 class="address">107140, г. Москва, ул. Русаковская,<br>д.13, БЦ Бородино Плаза</h3></div>
                </div>
                <div class="row">
                    <div class="col-sm-12 social"><img src="assets/context/social/google-plus.png"> <img
                                src="assets/context/social/vk.png"> <a href="https://www.facebook.com/readymadesmobile/"
                                                                       target="_blank"><img
                                    src="assets/context/social/facebook.png"></a></div>
                </div>
                <div class="copyright text-uppercase text-center"><img src="assets/context/coffee-man.png"><h5
                            class="core-sans-a35-light text-normal">© all rights reserved</h5></div>
            </footer>
        </div>
    </div>
<?=$this->render('menu', ['style' => 'white']);?>