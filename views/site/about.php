<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$this->title = 'О нас';
?>
    <div class="container-fluid main">
        <header class="main-header blue text-uppercase">
            <div class="logo-title content-indent">
                <h3>
                    <a href="/">
                        центр&#13;&#10;мобильной <br/> разработки
                    </a>
                    <span class="main-menu-toggle visible-xs pull-right">
                            <svg viewBox="0 0 800 600">
                            <path d="M300,220 C300,220 520,220 540,220 C740,220 640,540 520,420 C440,340 300,200 300,200" id="top"></path>
                            <path d="M300,320 L540,320" id="middle"></path>
                            <path d="M300,210 C300,210 520,210 540,210 C740,210 640,530 520,410 C440,330 300,190 300,190" id="bottom" transform="translate(480, 320) scale(1, -1) translate(-480, -318) "></path>
                            </svg>
                        </span>
                </h3>
            </div>
            <hr />
            <div class="header-logo-container text-center">
                <a href="/" class="hidden-xs">главная</a>
                <div class="header-logo">
                    <img src="assets/context/ready-mades-01-2.png"/>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 text-uppercase text-center">
                    <h1 class="header-title">о нас</h1>
                </div>
            </div>
        </header>
        <div class="main-content">
            <section class="content-indent">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="core-sans-a35-light text-center">
                            Мы знаем, что работа в команде, при разработке сложных инновационных
                            продуктов&nbsp;-&nbsp;это сложный процесс. Мы знаем сколько автотестов прогояется
                            на&nbsp;устройстве в&nbsp;день, сколько задач попадает в&nbsp;сервисдеск
                            и&nbsp;сколько команда&nbsp;iOS&nbsp;выпивает редбула перед релизом.
                        </p>
                    </div>
                </div>
            </section>

            <section class="team content-indent">
                <div class="row">
                    <div class="col-sm-4 text-center">
                        <h1 class="core-sans-a25-extra-light big red" data-grow="60">> <span></span>
                        </h1>
                        <p class="core-sans-a55-medium small">
                            Именно столько сотрудников<br />
                            работает в центре: руководители<br />
                            проектов, разработчики, тестировщики,<br />
                            UI/UX проектировщики, дизайнеры,<br />
                            аналитики.
                        </p>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h1 class="core-sans-a25-extra-light big red" data-grow="55"><span></span>+</h1>
                        <p class="core-sans-a55-medium small">
                            IT-проектов, в которых приняли<br />
                            участие наши специалисты.<br />
                            Логистика, ритейл, финансы,<br />
                            здравоохранение, сфера услуг,<br />
                            gamedev.
                        </p>
                    </div>
                    <div class="col-sm-4 text-center">
                        <h1 class="core-sans-a25-extra-light big red" data-grow="7.5"><span></span> лет</h1>
                        <p class="core-sans-a55-medium small">
                            Средний стаж работы нашей<br />
                            команды. Эффективное сочетание<br />
                            теоретической подкованности<br />
                            и практических компетенций.<br />
                        </p>
                    </div>
                </div>

                <div class="person-container">
                    <?php if(count($employees) > 0):?>
                        <?php foreach ($employees as $employee):?>
                            <figure class="person">
                                <p><?= Html::img($employee->getImageFileUrl('photo'), ['width' => 230]);?></p>
                                <figcaption>
                                    <span><?= $employee->fullname;?><br />
                                    <?= $employee->post;?></span>
                                    <div class="details"><?= $employee->description;?></div>
                                </figcaption>
                            </figure>
                        <?php endforeach;?>
                    <?php endif;?>
                </div>
                <div class="be-with-us">
                    <figure class="person">
                        <h1 class="core-sans-a65-bold text-center">+</h1>
                        <p class="core-sans-a25-extra-light text-center text-uppercase">будь с нами !</p>
                    </figure>
                </div>
            </section>

            <footer class="main-footer content-indent">
                <div class="footer-logo-container text-center">
                    <div class="footer-logo">
                        <img src="assets/context/ready-mades-01-7.png"/>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 footer-text footer-left-text">
                        <h2 class="core-sans-a65-bold red">Всегда рады <br/> общению!</h2>
                    </div>
                    <div class="col-sm-6 footer-text footer-right-text core-sans-a25-extra-light">
                        <h2 class="footer-text-phone">+7 (969) 656 73 26</h2>
                        <h2 class="footer-text-email">welcomе@r-mades.com</h2>
                        <h3 class="address">400136, г.Волгоград, ул.&nbsp;Канунникова,<br/>д.23, БЦ Дельта</h3>
                        <h3 class="address">107140, г. Москва, ул. Русаковская,<br/>д.13, БЦ Бородино Плаза</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 social">
                        <img src="assets/context/social/google-plus.png" />
                        <img src="assets/context/social/vk.png" />
                        <a href="https://www.facebook.com/readymadesmobile/" target="_blank">
                            <img src="assets/context/social/facebook.png" />
                        </a>
                    </div>
                </div>
                <div class="copyright text-uppercase text-center">
                    <img src="assets/context/coffee-man.png"/>
                    <h5 class="core-sans-a35-light text-normal">© all rights reserved</h5>
                </div>
            </footer>
        </div>
    </div>
<?= $this->render('menu', ['style' => '']);